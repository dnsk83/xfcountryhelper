﻿using PhoneNumberKit.Helpers;
using PhoneNumberKit.Services;
using PhoneNumbers;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PhoneNumberKit
{
    public partial class MainPage : ContentPage
    {
        private string _region;
        private PhoneNumberUtil _util;

        public MainPage()
        {
            InitializeComponent();
            SetupPhoneField();
        }

        private void SetupPhoneField()
        {
            _util = PhoneNumberUtil.GetInstance();

            _region = "BY";

            var placehoder = _util.GetExampleNumberForType(_region, PhoneNumberType.MOBILE);
            Mask = _util.FormatOutOfCountryCallingNumber(placehoder, _region);
            phoneEntry.Placeholder = Mask;
            phoneLabel.Text = Mask;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var page = new CountriesListPage();
            await PopupNavigation.Instance.PushAsync(page);
        }

        private string _mask = "";
        public string Mask
        {
            get => _mask;
            set
            {
                _mask = value;
                SetPositions();
            }
        }

        private IDictionary<int, char> _maskNonDigits;

        private void SetPositions()
        {
            if (string.IsNullOrEmpty(Mask))
            {
                _maskNonDigits = null;
                return;
            }

            var result = new Dictionary<int, char>();
            for (var position = 0; position < Mask.Length; position++)
            {
                if (!char.IsDigit(Mask, position))
                {
                    result.Add(position, Mask[position]);
                }
            }
            _maskNonDigits = result;
        }

        private void PhoneEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = sender as Entry;
            var text = entry.Text;

            if (string.IsNullOrWhiteSpace(text) || _maskNonDigits == null)
                return;

            if (text.Length > _mask.Length)
            {
                entry.Text = text.Remove(text.Length - 1);
                return;
            }

            foreach (var position in _maskNonDigits)
            {
                if (text.Length >= position.Key + 1)
                {
                    var value = position.Value.ToString();
                    if (text.Substring(position.Key, 1) != value)
                    {
                        text = text.Insert(position.Key, value);
                    }
                }
            }

            if (entry.Text != text)
            {
                entry.Text = text;
            }
        }
    }
}
