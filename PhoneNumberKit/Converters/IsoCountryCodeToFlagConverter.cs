﻿using System;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;

namespace PhoneNumberKit.Converters
{
    public class IsoCountryCodeToFlagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var code = (string)value;
            return string.Concat(code.ToUpper().Select(x => char.ConvertFromUtf32(x + 0x1F1A5)));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
