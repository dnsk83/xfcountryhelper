﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneNumberKit.Services
{
    public interface ICountryHelper
    {
        List<KeyValuePair<string, string>> GetCountries();
    }
}
