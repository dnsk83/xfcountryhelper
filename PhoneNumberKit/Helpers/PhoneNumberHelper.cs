﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PhoneNumberKit.Helpers
{
    public class PhoneNumberHelper
    {
        public void AttachToEntry(Entry entry)
        {
            entry.Keyboard = Keyboard.Telephone;
        }
    }
}
