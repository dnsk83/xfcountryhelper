﻿using PhoneNumberKit.Services;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhoneNumberKit
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CountriesListPage : PopupPage
    {
        private readonly ICountryHelper _countryHelper;

        public CountriesListPage()
        {
            InitializeComponent();
            _countryHelper = DependencyService.Get<ICountryHelper>();

            BindingContext = this;

            _countryHelper.GetCountries().ForEach(Countries.Add);
            OnPropertyChanged(nameof(Countries));
        }

        public ObservableCollection<KeyValuePair<string, string>> Countries { get; } = new ObservableCollection<KeyValuePair<string, string>>();
        public KeyValuePair<string, string>? SelectedCountry {get; set;}

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}