﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;
using PhoneNumberKit.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(PhoneNumberKit.Droid.Services.CountryHelperAndroid))]
namespace PhoneNumberKit.Droid.Services
{
    public class CountryHelperAndroid : ICountryHelper
    {
        public List<KeyValuePair<string, string>> GetCountries()
        {
            var IsoCountryCodeLength = 2;
            var countries = Locale.GetAvailableLocales()
                .Where(l => !string.IsNullOrEmpty(l.DisplayCountry) && l.Country.Length == IsoCountryCodeLength)
                .Select(l => new KeyValuePair<string, string>(l.Country, l.DisplayCountry))
                .Distinct()
                .OrderBy(l => l.Value)
                .ToList();

            return countries;
        }
    }
}