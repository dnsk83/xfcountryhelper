﻿using System;
using System.Collections.Generic;
using Foundation;
using PhoneNumberKit.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(PhoneNumberKit.iOS.Services.CountryHelperIOS))]
namespace PhoneNumberKit.iOS.Services
{
    public class CountryHelperIOS : ICountryHelper
    {
        public List<KeyValuePair<string, string>> GetCountries()
        {
            var codes = NSLocale.ISOCountryCodes;
            var countries = new List<KeyValuePair<string, string>>();

            foreach (var code in codes)
            {
                var displayName = NSLocale.CurrentLocale.GetCountryCodeDisplayName(code);
                countries.Add(new KeyValuePair<string, string>(code, displayName));
            }

            countries.Sort(new ValueComparer());

            return countries;
        }
    }

    internal class ValueComparer : IComparer<KeyValuePair<string, string>>
    {
        public int Compare(KeyValuePair<string, string> x, KeyValuePair<string, string> y)
        {
            return string.Compare(x.Value, y.Value, true);
        }
    }
}
